package main

import (
    "bitbucket.org/uhallgarn/strtech/hellognatsd/person"
    "fmt"
	"github.com/apcera/nats"
	"os"
	"strings"
)


//------------------------------------------------------------

func main () {

	var (
		con		*nats.EncodedConn
		nc		*nats.Conn
		err		error
	)

    var persons = []person.PersonT{
            person.PersonT{"Ulf", "ulf@hallgarn.se"}, 
            person.PersonT{"Helena", "helena@hallgarn.se"},
            person.PersonT{"Gustav", "gustav@hallgarn.se"},
            person.PersonT{"Axel", "axel@hallgarn,se"}}

	opts := nats.DefaultOptions
	opts.MaxReconnect = -1 // Infinit reconnects.
	opts.Servers = strings.Split(nats.DefaultURL, ",")
	for i, s := range opts.Servers {
		opts.Servers[i] = strings.Trim(s, " ")
	}
	opts.Secure = false

	if nc, err = opts.Connect(); err != nil {
		fmt.Println("Can't connect to gnatsd")
		os.Exit(1)
	}
	if con, err = nats.NewEncodedConn(nc, "json"); err != nil {
		fmt.Println("Can't get json encoder from gnatsd")
		os.Exit(2)
	}
	
	personCtrlCh := make(person.PersonCtrlChT, 10)
	con.BindRecvChan("PersCtrlCh", personCtrlCh)
	personsCh := make(person.PersonsChT, 10)
	con.BindSendChan("PersCh", personsCh)

    for {
        select {
            case newCmd := <- personCtrlCh:
                switch newCmd.Cmd {
                    case "list":
                        fmt.Println("Person server, got list request")
                        personsCh <- persons
                }
        }
    }
}


