package main

import (
    "bitbucket.org/uhallgarn/strtech/hellognatsd/person"
	"encoding/json"
    "flag"
    "fmt"
	"github.com/gorilla/mux"
	"github.com/apcera/nats"
	"net/http"
	"os"
	"strings"
)

var (
    personCtrlCh    person.PersonCtrlChT
    personsCh       person.PersonsChT
)


//------------------------------------------------------------

func SayHello(w http.ResponseWriter, r *http.Request) {
    
    fmt.Println("SayHello: Got request")

    personCtrlCh <- person.PersonCtrlT{"list"}
    persons := <- personsCh
    
	result, _ := json.Marshal(persons)
    w.Write([]byte(result))
}

//------------------------------------------------------------

func main () {
    
	var (
		con		*nats.EncodedConn
		nc		*nats.Conn
		err		error
	)

	port := flag.String("p", "6789", "Port to use.")
	flag.Parse()

	opts := nats.DefaultOptions
	opts.MaxReconnect = -1 // Infinit reconnects.
	opts.Servers = strings.Split(nats.DefaultURL, ",")
	for i, s := range opts.Servers {
		opts.Servers[i] = strings.Trim(s, " ")
	}
	opts.Secure = false

	if nc, err = opts.Connect(); err != nil {
		fmt.Println("Can't connect to gnatsd")
		os.Exit(1)
	}
	if con, err = nats.NewEncodedConn(nc, "json"); err != nil {
		fmt.Println("Can't get json encoder from gnatsd")
		os.Exit(2)
	}
	
	personCtrlCh = make(person.PersonCtrlChT, 10)
	con.BindSendChan("PersCtrlCh", personCtrlCh)
	personsCh = make(person.PersonsChT, 10)
	con.BindRecvChan("PersCh", personsCh)
	
	r := mux.NewRouter()
	r.HandleFunc("/api/strtech/hello", SayHello).Methods("GET")
	if err := http.ListenAndServe(":"+*port, r); err != nil {
		panic("http.ListendAndServer() failed")
	}
}


