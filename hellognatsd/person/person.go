package person

type PersonT struct {
    Name    string  `json:"name"`
    Email   string  `json:"email"`
}
type PersonsChT chan []PersonT

type PersonCtrlT struct {
    Cmd         string
}
type PersonCtrlChT chan PersonCtrlT

var (
    personCtrlCh PersonCtrlChT
)

