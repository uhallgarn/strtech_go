package main

import (
    "flag"
	"github.com/gorilla/mux"
	"net/http"
)

//------------------------------------------------------------

func SayHello(w http.ResponseWriter, r *http.Request) {
    w.Write([]byte("Tjena Strängnäs Tech!"))
}

//------------------------------------------------------------

func main () {
	port := flag.String("p", "6789", "Port to use.")
	flag.Parse()
	
	r := mux.NewRouter()
	r.HandleFunc("/api/strtech/hello", SayHello).Methods("GET")
	if err := http.ListenAndServe(":"+*port, r); err != nil {
		panic("http.ListendAndServer() failed")
	}
}

