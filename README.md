# README #

This is different "Hello World!" examples in Googles GO (golang).
Written for Strängnäs Tech Community 2015-11-12.

The examples below is in some logical order starting with the simplest one and then expanding with more features.
To be able to run, you must have go on the machine, see http://goolang.org for more info.
When go is working on your machine, travers to different directories (as helloarr) and compile the code with 'go build'

When using helloweb and beond, the gorilla is needed, see http://http://www.gorillatoolkit.org/

When/if using the message bus example hellognatsd the gnatsd from Apcera is needed, see https://github.com/nats-io/gnatsd

I run on Linux/ubuntu, so my recommendation is to use some Linux distro. Have not tested on Windows since I think that is more for playing games and run office on... :-)

/Ulf


### hello ###

Simple "Hello World!" example. 


### helloarr ###

Using a string array and the range to iterate over this array.
Observe that range is returning to parameters (key, value) for every iteration, even if the key isn't used in my example (thats why it says _, name := ....)


### helloweb ###

Shows how this can act as a web server and be called from any browser. Compile and start (you can use flag p if running on other port than default 6789).
When program run, use a browser and write http://<ip-address>:6789/api/strtech/hello and this will resopond. The ip-address is to the machine where you are running the go code.


### hellochan ###

Same as helloweb, but I have splitted the code internally so that there will be one gorutine keeping the data for the persons, and then another (the main) rutine that acts as the router and listen to the port. These two gorutines communicates with each others using channels.
Compile, start and use browser as in same way as in helloweb


### hellojson ###

Extension of hellochan to show how easy it is to use json (JavaScript Object Notation), so now we have a web server/service with a REST/JSON-based interface.

### hellognatsd ###

There are three subdirectories, one (the person) just keep info about the different declarations of types for Person that the are common for the other two.
The subdirectory webserver, is the webserver itself. It listens to port 6789 for some request. When this request it uses the message bus to request for the information about persons.
The subdirectory personserver, is the (now) separate process for handling the data, the information about Persons. It listen to the message bus and when geting a requests it returns the data.

So, there are now to separate physical processes, not as in previous example with just one process and two rutines. The communication between these processes are done over the message bus, but observe how nice the go channels can be integrated with channels in the message bus.

First the gnatsd has to be started and be up running.
Move to the webserver directory and compile. When done start the webserver.
Move to the personserver directory, compile and start.
Now use the browser in tha same way as the examples for helloweb and make the request.