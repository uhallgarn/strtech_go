package main

import "fmt"

var names = []string{"Ulf", "Helena", "Gustav", "Axel"}

func main() {
    fmt.Print("Hello,")
    for _, name := range names {
        fmt.Print(" " + name)
    }
    fmt.Println("!")
}