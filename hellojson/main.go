package main

import (
	"encoding/json"
    "flag"
	"github.com/gorilla/mux"
	"net/http"
)

type PersonT struct {
    Name    string  `json:"name"`
    Email   string  `json:"email"`
}
type PersonsChT chan []PersonT

type PersonCtrlT struct {
    Cmd         string
    PersonsCh   PersonsChT
}
type PersonCtrlChT chan PersonCtrlT

var (
    personCtrlCh PersonCtrlChT
)


//------------------------------------------------------------

func persons(personCtrlCh PersonCtrlChT) {
    
    var persons = []PersonT{
            PersonT{"Ulf", "ulf@hallgarn.se"}, 
            PersonT{"Helena", "helena@hallgarn.se"},
            PersonT{"Gustav", "gustav@hallgarn.se"},
            PersonT{"Axel", "axel@hallgarn,se"}}
    
    for {
        select {
            case newCmd := <- personCtrlCh:
                switch newCmd.Cmd {
                    case "list":
                        newCmd.PersonsCh <- persons
                }
        }
    }
}

//------------------------------------------------------------

func SayHello(w http.ResponseWriter, r *http.Request) {

	personsCh := make(PersonsChT, 10)
    personCtrlCh <- PersonCtrlT{"list", personsCh}
    persons := <- personsCh
    
	result, _ := json.Marshal(persons)
    w.Write([]byte(result))
}

//------------------------------------------------------------

func main () {
	port := flag.String("p", "6789", "Port to use.")
	flag.Parse()
	
	personCtrlCh = make(PersonCtrlChT, 10)
    go persons(personCtrlCh)	
	
	r := mux.NewRouter()
	r.HandleFunc("/api/strtech/hello", SayHello).Methods("GET")
	if err := http.ListenAndServe(":"+*port, r); err != nil {
		panic("http.ListendAndServer() failed")
	}
}


